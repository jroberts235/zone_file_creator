#!/usr/bin/env ruby
require 'erb'

module NamedAppender
    class RenderNamedFile
        include ERB::Util
        attr_accessor :template
        attr_accessor :domain_name
        attr_accessor :filename

        def initialize(args)
            @domain_name = args[0]
            @filename = args[1]
            @template = self.get_zone_template
        end

        def render
             renderer = ERB.new(@template, 0, '-').result(binding)
        end

        def save
            File.open(@filename, "a") do |f|
                f.write(render)
            end
        end

        def print
            puts render
        end

        def get_zone_template
          %{zone "<%= @domain_name %>" {
                    type master;
                    file "<%= @domain_name %>.db";
                    notify no;
          };
          }

        end
        {:success => true}
    end  # end of class

    if __FILE__ == $0
       begin
           named_local_file = '/etc/bind/named.conf.local'
           domain_name = ARGV[0] ||= abort('failed')
           args = [domain_name, named_local_file]
           named_entry = RenderNamedFile.new(args)
           named_entry.print # print to stdout
           named_entry.save()
       rescue => e
           abort 'failed'
       end
    end
end
