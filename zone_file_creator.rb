#!/usr/bin/env ruby
require 'erb'

module ZonefileCreator
    class RenderZoneFile
        include ERB::Util
        attr_accessor :template
        attr_accessor :domain_name
        attr_accessor :filename
    
        def initialize(args)
            @template = self.get_zone_template(domain_name)
            @domain_name = args[0]
            @zone_file_dir = args[1]
            @filename = "#{@zone_file_dir}/#{@domain_name}.db"
        end
    
        def render
             renderer = ERB.new(@template, 0, '-').result(binding)
        end
    
        def save
            File.open(@filename, "w+") do |f|
                f.write(render)
            end
        end
    
        def print
            puts render
        end
    
        def get_zone_template(domain_name)
            %{@       IN      SOA    ns.<%= @domain_name %>. <%= @domain_name %>.  (
                                        1         ; Serial
                                   604800         ; Refresh
                                    86400         ; Retry
                                  2419200         ; Expire
                                      30 )        ; Negative Cache TTL
            ;
            @       IN      NS      localhost.
    
            www             A       127.0.0.1 }
        end
        {:success => true}
    end  # end of class

    if __FILE__ == $0
       begin
           zone_file_dir = 'zones'
           domain_name = ARGV[0] ||= abort('failed')
           args = ['domain_name', 'zone_file_dir']
           zone_file = RenderZoneFile.new(args)
           #zone_file.print # print to stdout
           zone_file.save()
       rescue => e
           abort 'failed'
       end
    end
end
